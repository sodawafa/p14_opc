import './assets/styles/style.css'
import React from 'react'
import Header from './components/Header'
import { CreateEmployee } from './containers/CreateEmployee'
import {
  Switch,
  Route,
  HashRouter,
} from 'react-router-dom'
import { EmployeeList } from './containers/EmployeeList'
import { Provider } from 'react-redux'
import store from './store'

function App () {
  return (
    <div className="App">
      <Provider store={store}>
        <Header/>
        <HashRouter>
          <main>
            <Switch>
              <Route exact path="/">
                <CreateEmployee/>
              </Route>
              <Route path="/employee-list">
                <EmployeeList/>
              </Route>
            </Switch>
          </main>
        </HashRouter>
      </Provider>
    </div>

  )
}

export default App
