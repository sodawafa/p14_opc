import React from 'react'

class Header extends React.Component {
  render () {
    return (
      <header>
        <div className="title">
          <h1>HRnet</h1>
        </div>
      </header>
    )
  }
}

export default Header
