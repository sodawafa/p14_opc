import { ACTION_TYPES } from '../constants'

//localStorage.setItem('employees', JSON.stringify([]))
const employees = JSON.parse(localStorage.getItem('employees')) || []

const initialState = { employees }

export function employee (state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.ADD_EMPLOYEE_SUCCESS:
      return {
        ...state,
        employees: action.employees,
      }
    case ACTION_TYPES.LIST_EMPLOYEE_SUCCESS:
      return {
        ...state,
        employees: action.employees,
      }
    default:
      return state
  }
}
