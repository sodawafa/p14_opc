import { combineReducers } from "redux";

import { employee } from './employee.reducer';

const reducers = combineReducers({
  employee,
});

export default reducers;
