import React from 'react'
import { Link } from 'react-router-dom'
import { STATES, DEPARTMENTS } from '../constants/definitionsTypes'
import { connect } from 'react-redux'
import { employeeActions } from '../actions'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'

import Modal from 'modal-library'
import 'modal-library/dist/index.css'

class CreateEmployee extends React.Component {

  variant = 'outlined'
  state = {
    modal: false,
  }

  constructor (props) {
    super(props)
    this.saveEmployee = this.saveEmployee.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.openModal = this.openModal.bind(this)
  }

  saveEmployee (e) {
    e.preventDefault()
    const employee = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      dateOfBirth: this.state.dateOfBirth,
      startDate: this.state.startDate,
      department: this.state.department || DEPARTMENTS[0],
      street: this.state.street,
      city: this.state.city,
      state: this.state.state || STATES[0].abbreviation,
      zipCode: this.state.zipCode,
    }
    this.props.dispatch(employeeActions.addEmployee(employee))
    this.openModal()
  }

  validate (value, longueur) {
    if (!value || value.length < longueur) {
      return value
    }
    return false
  }

  handleChange (e) {
    const { name, value } = e.target
    this.setState({
      [name]: value,
    })
  }

  openModal (e) {
    this.setState({ modal: true })
  }

  closeModal () {
    this.setState({ modal: false })
  }

  render () {
    return (
      <main>
        <div className="container">
          <Link to="employee-list"
                className={'btn'}>View
            Current Employees</Link>
          {/*<button className={'modal-btn'} onClick={this.openModal}>open modal
          </button>*/}
          <Modal modalFn={false}
                 modalBtnOpen={false}
                 modalBtnClose={true}
                 modal={this.state.modal}
                 top={'20%'}
                 style={{ margin: '1em 143px' }}
                 openModal={this.openModal.bind(this)}
                 closeModal={this.closeModal.bind(this)}>
            <p>Employee Created!</p>
          </Modal>
          <h2>Create Employee</h2>
          <form action="#" id="create-employee" onSubmit={this.saveEmployee}>

            <TextField label="First Name" variant={this.variant}
                       id="first-name" name={'firstName'}
                       onChange={this.handleChange}/>
            <TextField label="Last Name" variant={this.variant}
                       id="last-name" name={'lastName'}
                       onChange={this.handleChange}/>

            <TextField
              label="Date of Birth"
              type="date"
              id="date-of-birth"
              name={'dateOfBirth'}
              variant={this.variant}
              onChange={this.handleChange}
              InputLabelProps={{
                shrink: true,
              }}
            />

            <TextField
              label="Start Date"
              type="date"
              id="start-date"
              name={'startDate'}
              variant={this.variant}
              onChange={this.handleChange}
              InputLabelProps={{
                shrink: true,
              }}
            />

            <fieldset className="address">
              <legend>Address</legend>

              <TextField
                variant={this.variant}
                type={'text'}
                label="Street"
                id="street"
                name={'street'}
                onChange={this.handleChange}/>

              <TextField
                variant={this.variant}
                type={'text'}
                label="City"
                id="city"
                name={'city'}
                onChange={this.handleChange}/>

              <FormControl>
                <Select
                  label={'State'}
                  name="state"
                  id="state"
                  defaultValue={STATES[0].abbreviation}
                  onChange={this.handleChange}
                  variant={this.variant}
                >
                  {
                    STATES.map((state, index) => {
                      return (
                        <MenuItem value={state.abbreviation} key={index}>
                          {state.name}
                        </MenuItem>
                      )
                    })
                  }
                </Select>
              </FormControl>

              <TextField
                variant={this.variant}
                type="number"
                label="Zip Code"
                id="zip-code"
                name={'zipCode'}
                onChange={this.handleChange}/>

            </fieldset>

            <FormControl>
              <Select
                label={'Department'}
                name="department"
                id="department"
                defaultValue={DEPARTMENTS[0]}
                onChange={this.handleChange}
                variant={this.variant}
              >
                {DEPARTMENTS.map((departement, index) => {
                  return (
                    <MenuItem value={departement} key={index}>
                      {departement}
                    </MenuItem>

                  )
                })}
              </Select>
            </FormControl>

            <br/>
            <Button className={'btn'}
                    type={'submit'}
                    style={{color:'white',
                    fontSize:20}}
                    variant="outlined">Save</Button>
            <br/>
          </form>

        </div>

      </main>
    )
  }
}

const mapStateToProps = state => ({
  employees: state.employee.employees,
})

const page = connect(mapStateToProps)(CreateEmployee)
export { page as CreateEmployee }
