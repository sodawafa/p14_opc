import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { DataGrid } from '@material-ui/data-grid'
import { employeeActions } from '../actions'
/*import Button from '@material-ui/core/Button'*/

class EmployeeList extends React.Component {

  columns = [
    { field: 'firstName', headerName: 'First name', width: 130 },
    { field: 'lastName', headerName: 'Last name', width: 130 },
    { field: 'startDate', headerName: 'Start Date', width: 130 },
    { field: 'department', headerName: 'Department', width: 130 },
    { field: 'dateOfBirth', headerName: 'Date of Birth', width: 130 },
    { field: 'street', headerName: 'Street', width: 130 },
    { field: 'city', headerName: 'City', width: 130 },
    { field: 'state', headerName: 'State', width: 90 },
    { field: 'zipCode', headerName: 'Zip Code', type: 'number', width: 110 },
  ]

  componentDidMount () {
    if (this.props.employees === undefined) {
      this.props.dispatch(employeeActions.listEmployees())
    }
  }

  render () {
    return (
      <div id="employee-div" className="container">
        <h2>Current Employees</h2>
        <div style={{ height: 400, width: '100%' }}>
          <DataGrid rows={this.props.employees}
                    columns={this.columns}
                    pageSize={5}
                    id="employee-table"
          />
        </div>
        <Link to="/"
              className={'btn'}>Home</Link>
        {/*<Button style={{background: 'linear-gradient(to bottom right, #3d4904f0,#d9fe2ad4)'}} variant="outlined" color="default" href="#/">Home</Button>*/}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  employees: state.employee.employees,
})

const EmployeeListPage = connect(mapStateToProps)(EmployeeList)
export { EmployeeListPage as EmployeeList }
