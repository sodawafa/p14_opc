import { ACTION_TYPES } from '../constants'
import store from '../store'

export const employeeActions = {
  addEmployee,
  listEmployees,
}

function addEmployee (employee) {
  return dispatch => {
    try {
      dispatch(request())
      let employees = store.getState().employee.employees || []
      employees.push({
        id: employees.length,
        ...employee,
      })
      localStorage.setItem('employees', JSON.stringify(employees))
      dispatch(success(employees))
    } catch (e) {
      dispatch(failure(e))
    }
  }

  function request () {
    return {
      type: ACTION_TYPES.ADD_EMPLOYEE_REQUEST,
      isSubmit: true,
    }
  }

  function success (employees) {
    return {
      type: ACTION_TYPES.ADD_EMPLOYEE_SUCCESS,
      employees,
    }
  }

  function failure (error) {
    return {
      type: ACTION_TYPES.ADD_EMPLOYEE_FAILURE,
      error,
    }
  }
}

function listEmployees () {
  return dispatch => {
    try {
      let employees = store.getState().employee.employees
      /*console.log('store:', employees)*/
      if (!employees || employees.length === 0) {
        employees = JSON.parse(localStorage.getItem('employees'))
        /*console.log('localStorage:', employees)*/
      }

      dispatch(success(employees))
    } catch (e) {
      dispatch(failure(e))
    }
  }

  function success (employees) {
    return {
      type: ACTION_TYPES.LIST_EMPLOYEE_SUCCESS,
      employees,
    }
  }

  function failure (error) {
    return {
      type: ACTION_TYPES.LIST_EMPLOYEE_FAILURE,
      error,
    }
  }
}
